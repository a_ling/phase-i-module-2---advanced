package com.lagou.edu.annotation;

import java.lang.annotation.*;

/**
 * @Author : liuchangling
 * @Descrition :
 * @Date： Created in 5:19 下午 2021/9/20
 */
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autowired {

    boolean required() default true;

}
