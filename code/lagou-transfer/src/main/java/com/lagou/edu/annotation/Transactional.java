package com.lagou.edu.annotation;

/**
 * @Author : liuchangling
 * @Descrition :
 * @Date： Created in 5:20 下午 2021/9/20
 */

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Transactional {
    String value() default "TransactionManager";
}
