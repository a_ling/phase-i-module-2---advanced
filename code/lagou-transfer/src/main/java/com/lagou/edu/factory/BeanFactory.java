package com.lagou.edu.factory;


import com.alibaba.druid.util.StringUtils;
import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.annotation.Service;
import com.lagou.edu.annotation.Transactional;
import org.reflections.Reflections;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author : liuchangling
 * @Descrition :
 * @Date： Created in 5:15 下午 2021/9/20
 */
public class BeanFactory {

    /**
     * 任务一：扫描包，分析需实例化的对象列表（service和事务管理器），通过反射技术实例化对象并且存储待用（map集合）
     * 任务二：对外提供获取实例对象的接口（根据id获取）
     */

    private static Map<String, Object> map = new HashMap<>();  // 存储对象


    static {
        try {
            // 任务一：扫描包，分析需实例化的对象列表（service和事务管理器）,通过反射技术实例化对象并且存储待用（map集合）
            //扫描获取反射对象集合
            Reflections f = new Reflections("com.lagou.edu");
            //获取注解为service的集合
            Set<Class<?>> set = f.getTypesAnnotatedWith(Service.class);

            for (Class<?> c : set) {
                // 通过反射技术实例化对象
                Object bean = c.newInstance();
                Service annotation = c.getAnnotation(Service.class);

                //对象ID在service注解有value时用value，没有时用类名
                if (StringUtils.isEmpty(annotation.value())) {
                    //由于getName获取的是全限定类名，所以要分割去掉前面包名部分
                    String[] names = c.getName().split("\\.");
                    map.put(names[names.length - 1], bean);
                } else {
                    map.put(annotation.value(), bean);
                }
            }

            // 实例化完成之后维护对象的依赖关系，检查哪些对象需要传值进入，根据它的配置，我们传入相应的值
            for (Map.Entry<String, Object> a : map.entrySet()) {
                Object o = a.getValue();
                Class c = o.getClass();
                //获取属性集合
                Field[] fields = c.getDeclaredFields();
                //遍历属性，若持有Autowired注解则注入
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Autowired.class)
                            && field.getAnnotation(Autowired.class).required()) {
                        String[] names = field.getType().getName().split("\\.");
                        String name = names[names.length - 1];
                        Method[] methods = c.getMethods();
                        for (int j = 0; j < methods.length; j++) {
                            Method method = methods[j];
                            if (method.getName().equalsIgnoreCase("set" + name)) {  // 该方法就是 setAccountDao(AccountDao accountDao)
                                method.invoke(o, map.get(name));
                            }
                        }
                    }
                }
                //判断对象类是否持有Transactional注解，若有则修改对象为代理对象
                if (c.isAnnotationPresent(Transactional.class)) {
                    //获取代理工厂
                    ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("proxyFactory");
                    //判断对象是否实现接口
                    Class[] face = c.getInterfaces();//获取类c实现的所有接口
                    if (face != null && face.length > 0) {
                        //实现使用JDK
                        o = proxyFactory.getJdkProxy(o);
                    } else {
                        //没实现使用CGLIB
                        o = proxyFactory.getCglibProxy(o);
                    }
                }

                // 把处理之后的object重新放到map中
                map.put(a.getKey(), o);
            }


        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }


    // 任务二：对外提供获取实例对象的接口（根据id获取）
    public static Object getBean(String id) {
        return map.get(id);
    }

}
